/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, {useState} from 'react';
import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';
import { Device } from 'react-native-ble-plx';
import { LineChart } from 'react-native-chart-kit';

import DeviceModal from './DeviceConnectionModal';
import useBLE from './useBLE';

const App = () => {

  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const {requestPermissions, scanForDevices, allDevices, connectToDevice, connectedDevice, disconnectFromDevice, temperature, humidity} = useBLE();


  const hideModal = () => {
    setIsModalVisible(false);
  };

  const openModal = async () => {
    requestPermissions((isGranted: boolean)=>{
      if(isGranted){
        scanForDevices();
        setIsModalVisible(true);
      }
    });
    
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.heartRateTitleWrapper}>
        {
          connectedDevice ? (
            <>
              <Text style={styles.heartRateTitleText}> Measured values:</Text>
              <Text style={styles.heartRateText}> Temperature: {temperature} °C</Text>
              <Text style={styles.heartRateText}> Humidity: {humidity} %</Text>
            </>
          ):(
            <Text style={styles.heartRateTitleText}>
            Please Connect to a BLE DAQ
            </Text>
          )
        }
        
        {allDevices.map((device: Device) => (
          <Text key={device.id}>{device.name}</Text>
        ))

        }
      </View>
      <TouchableOpacity onPress={connectedDevice ? disconnectFromDevice : openModal} style={styles.ctaButton}>
        <Text style={styles.ctaButtonText}>
          {connectedDevice ? "Disconnect" : "Connect"}
          </Text>
      </TouchableOpacity>
      <DeviceModal closeModal={hideModal} visible={isModalVisible} connectToPeripheral={connectToDevice} devices={allDevices}/>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f2f2f2',
  },
  heartRateTitleWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  heartRateTitleText: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    marginHorizontal: 20,
    color: 'black',
  },
  heartRateText: {
    fontSize: 25,
    marginTop: 15,
  },
  ctaButton: {
    backgroundColor: 'purple',
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    marginHorizontal: 20,
    marginBottom: 5,
    borderRadius: 8,
  },
  ctaButtonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
  },
});

export default App;