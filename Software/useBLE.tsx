import {useState} from 'react';
import { PermissionsAndroid, Platform } from "react-native";
import {
    BleError,
    BleManager,
    Characteristic,
    Device,
} from 'react-native-ble-plx';
import {PERMISSIONS, requestMultiple} from 'react-native-permissions';
import DeviceInfo from 'react-native-device-info';
import RNFS from 'react-native-fs';

import {atob} from 'react-native-quick-base64';
import base64 from 'react-native-base64';

global.Buffer = require('buffer').Buffer;


type PermissionCallBack = (result: boolean) => void;

const bleManager = new BleManager();

const CTRL_UUID = "00006910-CC7A-482A-984A-7F2ED5B3E58F";
const CTRL_CHARACTERISTIC = "00006911-8E22-4541-9D4C-21EDAE82ED19";

const SENSOR_UUID = "00006920-CC7A-482A-984A-7F2ED5B3E58F";
const TEMP_CHARACTERISTIC = "00006921-8E22-4541-9D4C-21EDAE82ED19";
const HUM_CHARACTERISTIC = "00006922-8E22-4541-9D4C-21EDAE82ED19";

interface BluetoothLowEnergyApi{
    requestPermissions(callback: PermissionCallBack): Promise<void>;
    scanForDevices(): void;
    allDevices: Device[];
    connectToDevice: (deviceId: Device) => Promise<void>;
    disconnectFromDevice: () => void;
    connectedDevice: Device | null;
    temperature: number;
    humidity: number;
}

function useBLE(): BluetoothLowEnergyApi {
    const [allDevices, setAllDevices] = useState<Device[]>([]);
    const [connectedDevice, setConnectedDevice] = useState<Device | null>(null);
    const [temperature, setTemperature] = useState<number>(0);
    const [humidity, setHumidity] = useState<number>(0);

    const requestPermissions = async (callback: PermissionCallBack) =>{
        if(Platform.OS === 'android'){

            const apiLevel = await DeviceInfo.getApiLevel();
            if(apiLevel < 31){
                const grantedStatus1 = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    {
                        title: "Location Permission",
                        message: "BLE needs location permission",
                        buttonNegative: "Cancel",
                        buttonPositive: "Ok",
                        buttonNeutral: "Maybe latter"
                    },
                );
                const grantedStatus2 = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    {
                        title: 'Storage Permission',
                        message: 'App needs access to storage to save data.',
                        buttonNeutral: 'Ask Me Later',
                        buttonNegative: 'Cancel',
                        buttonPositive: 'OK',
                    });
                callback(grantedStatus1 === PermissionsAndroid.RESULTS.GRANTED && grantedStatus2 === PermissionsAndroid.RESULTS.GRANTED);
            } else{
                const result = await requestMultiple([
                    PERMISSIONS.ANDROID.BLUETOOTH_SCAN,
                    PERMISSIONS.ANDROID.BLUETOOTH_CONNECT,
                    PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
                    PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
                ]);
                const isAllGranted = 
                    result['android.permission.BLUETOOTH_SCAN'] === PermissionsAndroid.RESULTS.GRANTED &&
                    result['android.permission.BLUETOOTH_CONNECT'] === PermissionsAndroid.RESULTS.GRANTED &&
                    result['android.permission.ACCESS_FINE_LOCATION'] === PermissionsAndroid.RESULTS.GRANTED &&
                    result['android.permission.WRITE_EXTERNAL_STORAGE'] === PermissionsAndroid.RESULTS.GRANTED
                callback(isAllGranted);
            }

            
        }else{
            callback(true);
        }
    };

    const isDuplicateDevice = (devices: Device[], nextDevice: Device) =>
        devices.findIndex((device) => device.id === nextDevice.id) > -1;

    const scanForDevices = () =>{
        bleManager.startDeviceScan(null, null, (error, device) =>{
            if(error){
                console.log(error);
            }
            if(device && device.name?.includes('Blu')){
                setAllDevices((prevState: Device[])=>{
                    if(!isDuplicateDevice(prevState, device)){
                        return[...prevState, device];
                    }
                    return prevState;
                });
            }
        });
    };

    const connectToDevice = async (device: Device) => {
        try{
            const deviceConnection = await bleManager.connectToDevice(device.id)
            setConnectedDevice(deviceConnection);
            await deviceConnection.discoverAllServicesAndCharacteristics();
            bleManager.stopDeviceScan()
            startStreamingData(deviceConnection);
        } catch (e){
            console.log("Error in connection", e);
        }
    };

    const disconnectFromDevice = () => {
        if(connectedDevice){
            bleManager.cancelDeviceConnection(connectedDevice.id);
            setConnectedDevice(null);
            setHumidity(0);
            setTemperature(0);
        }
    };

    const logData = async (temp: number | null, hum: number | null) => {

        const logEntry = `Temperature: ${temp !== null ? temp.toFixed(2) : 'N/A'}°C, Humidity: ${hum !== null ? hum.toFixed(2) : 'N/A'}%, Timestamp: ${new Date().toISOString()}\n`;
        const filePath = `${RNFS.DownloadDirectoryPath}/temp_hum.log`;
        console.log('Path:', filePath);
        try {
            
            //await RNFileAccess.appendFile('temperature_humidity_log.txt', logEntry);
            await RNFS.writeFile(filePath, logEntry, 'utf8');
          console.log('Data logged:', logEntry);
        } catch (error) {
          console.error('Failed to log data:', error);
        }
      };

    const onTempUpdate = (
        error: BleError | null,
        characteristic: Characteristic | null
    ) => {
        if (error) {
            console.log(error);
            return -1;
        }else if (!characteristic?.value){
            console.log("No data was received");
            return -1;
        }
        const rawData = Buffer.from(base64.decode(characteristic.value), 'binary');
        const tempData = ((rawData[0] << 8) | rawData[1]) / 100
        console.log("Raw data: ", tempData);

        logData(tempData, null);
        setTemperature(tempData);
    }

    const onHumUpdate = (
        error: BleError | null,
        characteristic: Characteristic | null
    ) => {
        if (error) {
            console.log(error);
            return -1;
        }else if (!characteristic?.value){
            console.log("No data was received");
            return -1;
        }
        const rawData = Buffer.from(base64.decode(characteristic.value), 'binary');
        const humData = ((rawData[0] << 8) | rawData[1]) / 100
        console.log("Raw data: ", humData);

        logData(null, humData);
        setHumidity(humData);
    }

    const startStreamingData = async (device: Device) => {
        if (device) {
            device.monitorCharacteristicForService(
              SENSOR_UUID,
              TEMP_CHARACTERISTIC,
              onTempUpdate
            );
            device.monitorCharacteristicForService(
                SENSOR_UUID,
                HUM_CHARACTERISTIC,
                onHumUpdate
              );
          } else {
            console.log("No Device Connected");
          }
    };

    return{
        requestPermissions,
        scanForDevices,
        allDevices,
        connectToDevice,
        disconnectFromDevice,
        connectedDevice,
        temperature,
        humidity,
    };
}

export default useBLE;