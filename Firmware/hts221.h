/*
 * hts221.h
 *
 *  Created on: Jun 29, 2024
 *      Author: Embedded Musician
 */

#ifndef INC_HTS221_H_
#define INC_HTS221_H_

#include "stdint.h"

uint8_t htsConfigure();
int16_t htsGetTemperatureRaw();
int16_t htsGetHumidityRaw();
uint8_t htsGetStatus();
void htsGetCalValues(float* cal_temp, float* cal_hum);
float htsGetTempF();
float htsGetHumF();

#endif /* INC_HTS221_H_ */
