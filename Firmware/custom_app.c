/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    App/custom_app.c
  * @author  MCD Application Team
  * @brief   Custom Example Application (Server)
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "app_common.h"
#include "dbg_trace.h"
#include "ble.h"
#include "custom_app.h"
#include "custom_stm.h"
#include "stm32_seq.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "hts221.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef struct
{
  /* BallzSettingsIn */
  /* SensorMeasurements */
  uint8_t               Tmeas_Notification_Status;
  uint8_t               Hmeas_Notification_Status;
  /* USER CODE BEGIN CUSTOM_APP_Context_t */
  uint8_t TimerMeasurement_Id;
  /* USER CODE END CUSTOM_APP_Context_t */

  uint16_t              ConnectionHandle;
} Custom_App_Context_t;

/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private defines ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define CUSTOM_MEASUREMENT_INTERVAL   (30*1000*1000/CFG_TS_TICK_VAL) // 30s
/* USER CODE END PD */

/* Private macros -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/**
 * START of Section BLE_APP_CONTEXT
 */

static Custom_App_Context_t Custom_App_Context;

/**
 * END of Section BLE_APP_CONTEXT
 */

uint8_t UpdateCharData[247];
uint8_t NotifyCharData[247];

/* USER CODE BEGIN PV */
uint8_t is_connected = 0;
uint16_t temperatureValue = 0;
uint8_t is_notification_enabled = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* BallzSettingsIn */
/* SensorMeasurements */
static void Custom_Tmeas_Update_Char(void);
static void Custom_Tmeas_Send_Notification(void);
static void Custom_Hmeas_Update_Char(void);
static void Custom_Hmeas_Send_Notification(void);

/* USER CODE BEGIN PFP */
static void Custom_Measurement(void);
static void Custom_Sample(void);
/* USER CODE END PFP */

/* Functions Definition ------------------------------------------------------*/
void Custom_STM_App_Notification(Custom_STM_App_Notification_evt_t *pNotification)
{
  /* USER CODE BEGIN CUSTOM_STM_App_Notification_1 */
	uint8_t payload[2];
  /* USER CODE END CUSTOM_STM_App_Notification_1 */
  switch (pNotification->Custom_Evt_Opcode)
  {
    /* USER CODE BEGIN CUSTOM_STM_App_Notification_Custom_Evt_Opcode */

    /* USER CODE END CUSTOM_STM_App_Notification_Custom_Evt_Opcode */

    /* BallzSettingsIn */
    case CUSTOM_STM_LSS_WRITE_EVT:
      /* USER CODE BEGIN CUSTOM_STM_LSS_WRITE_EVT */
    	HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    	payload[0] = pNotification->DataTransfered.pPayload[0];
    	payload[1] = pNotification->DataTransfered.pPayload[1];
    	/* Write the setting parametrisation*/
      /* USER CODE END CUSTOM_STM_LSS_WRITE_EVT */
      break;

    /* SensorMeasurements */
    case CUSTOM_STM_TMEAS_NOTIFY_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_TMEAS_NOTIFY_ENABLED_EVT */

    	is_notification_enabled |= 0x01;

      /* USER CODE END CUSTOM_STM_TMEAS_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_TMEAS_NOTIFY_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_TMEAS_NOTIFY_DISABLED_EVT */
    	is_notification_enabled &= ~(0x01);
      /* USER CODE END CUSTOM_STM_TMEAS_NOTIFY_DISABLED_EVT */
      break;

    case CUSTOM_STM_HMEAS_NOTIFY_ENABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_HMEAS_NOTIFY_ENABLED_EVT */
    	is_notification_enabled |= 0x02;
      /* USER CODE END CUSTOM_STM_HMEAS_NOTIFY_ENABLED_EVT */
      break;

    case CUSTOM_STM_HMEAS_NOTIFY_DISABLED_EVT:
      /* USER CODE BEGIN CUSTOM_STM_HMEAS_NOTIFY_DISABLED_EVT */
    	is_notification_enabled &= ~(0x02);
      /* USER CODE END CUSTOM_STM_HMEAS_NOTIFY_DISABLED_EVT */
      break;

    case CUSTOM_STM_NOTIFICATION_COMPLETE_EVT:
      /* USER CODE BEGIN CUSTOM_STM_NOTIFICATION_COMPLETE_EVT */

      /* USER CODE END CUSTOM_STM_NOTIFICATION_COMPLETE_EVT */
      break;

    default:
      /* USER CODE BEGIN CUSTOM_STM_App_Notification_default */

      /* USER CODE END CUSTOM_STM_App_Notification_default */
      break;
  }
  /* USER CODE BEGIN CUSTOM_STM_App_Notification_2 */

  /* USER CODE END CUSTOM_STM_App_Notification_2 */
  return;
}

void Custom_APP_Notification(Custom_App_ConnHandle_Not_evt_t *pNotification)
{
  /* USER CODE BEGIN CUSTOM_APP_Notification_1 */

  /* USER CODE END CUSTOM_APP_Notification_1 */

  switch (pNotification->Custom_Evt_Opcode)
  {
    /* USER CODE BEGIN CUSTOM_APP_Notification_Custom_Evt_Opcode */

    /* USER CODE END P2PS_CUSTOM_Notification_Custom_Evt_Opcode */
    case CUSTOM_CONN_HANDLE_EVT :
      /* USER CODE BEGIN CUSTOM_CONN_HANDLE_EVT */
    	is_connected = 1;
    	HW_TS_Stop(Custom_App_Context.TimerMeasurement_Id);
    	HW_TS_Start(Custom_App_Context.TimerMeasurement_Id, CUSTOM_MEASUREMENT_INTERVAL);
      /* USER CODE END CUSTOM_CONN_HANDLE_EVT */
      break;

    case CUSTOM_DISCON_HANDLE_EVT :
      /* USER CODE BEGIN CUSTOM_DISCON_HANDLE_EVT */
    	is_connected = 0;
    	HW_TS_Stop(Custom_App_Context.TimerMeasurement_Id);
      /* USER CODE END CUSTOM_DISCON_HANDLE_EVT */
      break;

    default:
      /* USER CODE BEGIN CUSTOM_APP_Notification_default */

      /* USER CODE END CUSTOM_APP_Notification_default */
      break;
  }

  /* USER CODE BEGIN CUSTOM_APP_Notification_2 */

  /* USER CODE END CUSTOM_APP_Notification_2 */

  return;
}

void Custom_APP_Init(void)
{
  /* USER CODE BEGIN CUSTOM_APP_Init */
	HW_TS_Create(CFG_TIM_PROC_ID_ISR, &(Custom_App_Context.TimerMeasurement_Id), hw_ts_Repeated, Custom_Sample);
  /* USER CODE END CUSTOM_APP_Init */
  return;
}

/* USER CODE BEGIN FD */

/* USER CODE END FD */

/*************************************************************
 *
 * LOCAL FUNCTIONS
 *
 *************************************************************/

/* BallzSettingsIn */
/* SensorMeasurements */
void Custom_Tmeas_Update_Char(void) /* Property Read */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Tmeas_UC_1*/
  updateflag = 1;
  /* USER CODE END Tmeas_UC_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_TMEAS, (uint8_t *)UpdateCharData);
  }

  /* USER CODE BEGIN Tmeas_UC_Last*/

  /* USER CODE END Tmeas_UC_Last*/
  return;
}

void Custom_Tmeas_Send_Notification(void) /* Property Notification */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Tmeas_NS_1*/
  updateflag = 1;
  /* USER CODE END Tmeas_NS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_TMEAS, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Tmeas_NS_Last*/

  /* USER CODE END Tmeas_NS_Last*/

  return;
}

void Custom_Hmeas_Update_Char(void) /* Property Read */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Hmeas_UC_1*/
  updateflag = 1;
  /* USER CODE END Hmeas_UC_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_HMEAS, (uint8_t *)UpdateCharData);
  }

  /* USER CODE BEGIN Hmeas_UC_Last*/

  /* USER CODE END Hmeas_UC_Last*/
  return;
}

void Custom_Hmeas_Send_Notification(void) /* Property Notification */
{
  uint8_t updateflag = 0;

  /* USER CODE BEGIN Hmeas_NS_1*/
  updateflag = 1;
  /* USER CODE END Hmeas_NS_1*/

  if (updateflag != 0)
  {
    Custom_STM_App_Update_Char(CUSTOM_STM_HMEAS, (uint8_t *)NotifyCharData);
  }

  /* USER CODE BEGIN Hmeas_NS_Last*/

  /* USER CODE END Hmeas_NS_Last*/

  return;
}

/* USER CODE BEGIN FD_LOCAL_FUNCTIONS*/

float temp_float = 0;
uint16_t temp_i16 = 0;

static void Custom_Sample(void){
	if((is_notification_enabled & 0x01) == 0x01){
		// Temperature notification is enabled
		temp_float = htsGetTempF();
		temp_i16 = (uint16_t)(temp_float*100);
		UpdateCharData[0] = (uint8_t)((temp_i16 >> 8) & 0x00FF);
		UpdateCharData[1] = (uint8_t)(temp_i16 & 0x00FF);
		Custom_Tmeas_Update_Char();
	}

	if((is_notification_enabled & 0x02) == 0x02){
			// Humidity notification is enabled
		temp_float = htsGetHumF();
		temp_i16 = (uint16_t)(temp_float*100);
		UpdateCharData[0] = (uint8_t)((temp_i16 >> 8) & 0x00FF);
		UpdateCharData[1] = (uint8_t)(temp_i16 & 0x00FF);
		Custom_Hmeas_Update_Char();
		}

	UTIL_SEQ_SetTask( 1<<1, CFG_SCH_PRIO_0);
}
/* USER CODE END FD_LOCAL_FUNCTIONS*/
