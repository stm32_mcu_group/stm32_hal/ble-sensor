/*
 * hts221.c
 *
 *  Created on: Jun 29, 2024
 *      Author: Embedded Musician
 */

/*
 * hts221.c
 *
 *  Created on: Mar 22, 2024
 *      Author: Embedded Musician
 */


#include "hts221.h"
#include "main.h"

#define HTS_ADDR	0xBE

#define HTS_WHO_AM_I	0x0F
#define HTS_ID			0xBC
#define	HTS_AV_CONF		0x10
#define HTS_CTR_REG1	0x20
#define HTS_CTR_REG2	0x21
#define HTS_CTR_REG3	0x22
#define HTS_STATUS_REG	0x27
#define HTS_HUM_OUT_L	0x28
#define HTS_HUM_OUT_H	0x29
#define HTS_TEMP_OUT_L	0x2A
#define HTS_TEMP_OUT_H	0x2B

#define HTS_H0_rH_x2	0x30
#define HTS_H1_rH_x2	0x31
#define HTS_T0_degC_x8	0x32
#define HTS_T1_degC_x8	0x33
#define HTS_T1_T0_MSB	0x35
#define	HTS_H0_T0_OUT1	0x36
#define HTS_H0_T0_OUT2	0x37
#define HTS_H1_T0_OUT1	0x3A
#define HTS_H1_T0_OUT2	0x3B
#define HTS_T0_OUT1		0x3C
#define HTS_T0_OUT2		0x3D
#define HTS_T1_OUT1		0x3E
#define HTS_T1_OUT2		0x3F

extern I2C_HandleTypeDef hi2c1;

#define hi2c hi2c1

static uint8_t  I2C_Read_Data(uint16_t Addr, uint8_t Reg){
	HAL_StatusTypeDef status = HAL_OK;
	uint8_t value = 0;
	status = HAL_I2C_Mem_Read(&hi2c, Addr, Reg, I2C_MEMADD_SIZE_8BIT, &value, 1, 0x10000);
	if(status != HAL_OK)
		__asm("NOP");
	return value;
}

static void I2C_Write_Data(uint16_t Addr, uint8_t Reg, uint8_t Value){
	HAL_I2C_Mem_Write(&hi2c, Addr, (uint16_t)Reg, I2C_MEMADD_SIZE_8BIT, &Value, 1, 0x10000);
}

uint8_t htsWhoAmI(){
	uint8_t data;
	data = I2C_Read_Data(HTS_ADDR, HTS_WHO_AM_I);
	if(data == HTS_ID)
		return HAL_OK;
	return HAL_ERROR;
}

uint8_t htsConfigure(){
	if(htsWhoAmI() == HAL_ERROR)
		return HAL_ERROR;
	// Control Reg 1
	// bit 7 - 0 power down / 1 active
	// bit 2 - 0 continuous / 1 block data update
	// bits 1/0 - 00 one-shot / 01 1Hz / 10 7Hz / 11 12.5Hz
	// Shut down
	I2C_Write_Data(HTS_ADDR, HTS_CTR_REG1, 0x05);
	// AV_CONF - resolution mode
	// bits 5 - 3 -- averaged temperature samples
	// bits 2 - 0 -- averaged humidity samples
	// Average 16 temperature and 32 humidity samples
	I2C_Write_Data(HTS_ADDR, HTS_AV_CONF, 0x1B);
	// Control Reg 2
	// bit 7 - 0 normal / 1 reboot memory
	// bit 1 - 0 disable heater / 1 enable heater
	// bit 0 - 0 wait for start of conversion / 1 start new dataset
	// Normal mode, waiting for conversion
	I2C_Write_Data(HTS_ADDR, HTS_CTR_REG2, 0);
	// Control Reg 3
	// bit 7 - drdy pin 0 active high / 1 active low
	// bit 6 - drdy is 0 push-pull / 1 open drain
	// bit 2 - drdy is 0 disabled / 1 enabled on pin
	// default mode, no drdy pin, since we did no connect it
	I2C_Write_Data(HTS_ADDR, HTS_CTR_REG3, 0x04);
	// Active mode, output updated after read, 1Hz data rate
	I2C_Write_Data(HTS_ADDR, HTS_CTR_REG1, 0x85);
	return HAL_OK;
}

void htsPowerDown(){
	I2C_Write_Data(HTS_ADDR, HTS_CTR_REG1, 0x05);
}

void htsPowerUp(){
	I2C_Write_Data(HTS_ADDR, HTS_CTR_REG1, 0x85);
}

uint8_t htsGetStatus(){
	uint8_t data;
	// STATUS REG bit 1 -- humidity data is available / bit 0 -- temperature data is available
	data = I2C_Read_Data(HTS_ADDR, HTS_STATUS_REG);
	return (data & 0x03);
}

int16_t htsGetTemperatureRaw(){
	// read High value last
	uint8_t dataH, dataL;
	int16_t result;
	dataL = I2C_Read_Data(HTS_ADDR, HTS_TEMP_OUT_L);
	dataH = I2C_Read_Data(HTS_ADDR, HTS_TEMP_OUT_H);
	result = (int16_t)(((uint16_t)(dataH) << 8) | ((uint16_t) dataL));
	return result;
}

int16_t htsGetHumidityRaw(){
	// read High value last
	uint8_t dataH, dataL;
	int16_t result;
	dataL = I2C_Read_Data(HTS_ADDR, HTS_HUM_OUT_L);
	dataH = I2C_Read_Data(HTS_ADDR, HTS_HUM_OUT_H);
	result = (int16_t)(((uint16_t)(dataH) << 8) | ((uint16_t) dataL));
	return result;
}

void htsGetCalValues(float* cal_temp, float* cal_hum){
	uint8_t T0_deg, T1_deg, T0_T1_MSB;
	int16_t T0_val, T1_val;
	int16_t T0_out, T1_out;

	int16_t H0_rh, H1_rh;
	int16_t H0_T0_out, H1_T0_out;

	uint8_t bufferL, bufferH;

	T0_deg = I2C_Read_Data(HTS_ADDR, HTS_T0_degC_x8);
	T1_deg = I2C_Read_Data(HTS_ADDR, HTS_T1_degC_x8);
	T0_T1_MSB = I2C_Read_Data(HTS_ADDR, HTS_T1_T0_MSB);

	T0_val = (((uint16_t)(T0_T1_MSB & 0x03)) << 8) | ((uint16_t) T0_deg);
	T1_val = (((uint16_t)(T0_T1_MSB & 0x0C)) << 6) | ((uint16_t) T1_deg);

	T0_val = T0_val >> 3;
	T1_val = T1_val >> 3;

	bufferL = I2C_Read_Data(HTS_ADDR, HTS_T0_OUT1);
	bufferH = I2C_Read_Data(HTS_ADDR, HTS_T0_OUT2);
	T0_out = (((uint16_t)bufferH) << 8) | ((uint16_t)bufferL);

	bufferL = I2C_Read_Data(HTS_ADDR, HTS_T1_OUT1);
	bufferH = I2C_Read_Data(HTS_ADDR, HTS_T1_OUT2);
	T1_out = (((uint16_t)bufferH) << 8) | ((uint16_t)bufferL);

	cal_temp[0] = ((float)(T1_val - T0_val)) / ((float)(T1_out - T0_out));
	cal_temp[1] = (float) T0_val - cal_temp[0] * ((float)T0_out);

	bufferL = I2C_Read_Data(HTS_ADDR, HTS_H0_rH_x2);
	bufferH = I2C_Read_Data(HTS_ADDR, HTS_H1_rH_x2);
	H0_rh = bufferL >> 1;
	H1_rh = bufferH >> 1;

	bufferL = I2C_Read_Data(HTS_ADDR, HTS_H0_T0_OUT1);
	bufferH = I2C_Read_Data(HTS_ADDR, HTS_H0_T0_OUT2);
	H0_T0_out = (((uint16_t)bufferH) << 8) | ((uint16_t) bufferL);

	bufferL = I2C_Read_Data(HTS_ADDR, HTS_H1_T0_OUT1);
	bufferH = I2C_Read_Data(HTS_ADDR, HTS_H1_T0_OUT2);
	H1_T0_out = (((uint16_t)bufferH) << 8) | ((uint16_t) bufferL);

	cal_hum[0] = ((float)(H1_rh - H0_rh)) / ((float)(H1_T0_out - H0_T0_out));
	cal_hum[1] = (float) H0_rh - cal_hum[0] * ((float)H0_T0_out);
}

float htsGetTempF(){
	float result;
	uint8_t buffer[4], tmp;
	int16_t T0_degC, T1_degC, T_out;
	int16_t T0_out, T1_out, T0_degC_x8_u16, T1_degC_x8_u16;
	// Read raw values
	buffer[0] = I2C_Read_Data(HTS_ADDR, HTS_T0_degC_x8);
	buffer[1] = I2C_Read_Data(HTS_ADDR, HTS_T1_degC_x8);
	tmp = I2C_Read_Data(HTS_ADDR, HTS_T1_T0_MSB);

	T0_degC_x8_u16 = (((uint16_t) (tmp & 0x03)) << 8) | ((uint16_t) buffer [0]);
	T1_degC_x8_u16 = (((uint16_t) (tmp & 0x0C)) << 6) | ((uint16_t) buffer [1]);

	T0_degC = T0_degC_x8_u16 >> 3;
	T1_degC = T1_degC_x8_u16 >> 3;

	buffer[0] = I2C_Read_Data(HTS_ADDR, HTS_T0_OUT1);
	buffer[1] = I2C_Read_Data(HTS_ADDR, HTS_T0_OUT2);
	buffer[2] = I2C_Read_Data(HTS_ADDR, HTS_T1_OUT1);
	buffer[3] = I2C_Read_Data(HTS_ADDR, HTS_T1_OUT2);

	T0_out = (((uint16_t) buffer [1]) << 8) | (uint16_t) buffer [0];
	T1_out = (((uint16_t) buffer [3]) << 8) | (uint16_t) buffer [2];

	buffer[0] = I2C_Read_Data(HTS_ADDR, HTS_TEMP_OUT_L);
	buffer[1] = I2C_Read_Data(HTS_ADDR, HTS_TEMP_OUT_H);

	T_out = (((uint16_t) buffer [1]) << 8) | (uint16_t) buffer [0];

	result = (float) (T_out - T0_out) * (float) (T1_degC - T0_degC) / (float) (T1_out - T0_out) + T0_degC;

	return result;
}

float htsGetHumF(){
	float result;
	uint8_t buffer[2];
	int16_t H0_T0_out, H1_T0_out, H_T_out;
	int16_t H0_rh, H1_rh;
	float tmp_f;

	buffer[0] = I2C_Read_Data(HTS_ADDR, HTS_H0_rH_x2);
	buffer[1] = I2C_Read_Data(HTS_ADDR, HTS_H1_rH_x2);

	H0_rh = buffer[0] >> 1;
	H1_rh = buffer[1] >> 1;

	buffer[0] = I2C_Read_Data(HTS_ADDR, HTS_H0_T0_OUT1);
	buffer[1] = I2C_Read_Data(HTS_ADDR, HTS_H0_T0_OUT2);

	H0_T0_out = (((uint16_t) buffer [1]) << 8) | (uint16_t) buffer [0];

	buffer[0] = I2C_Read_Data(HTS_ADDR, HTS_H1_T0_OUT1);
	buffer[1] = I2C_Read_Data(HTS_ADDR, HTS_H1_T0_OUT2);

	H1_T0_out = (((uint16_t) buffer [1]) << 8) | (uint16_t) buffer [0];

	buffer[0] = I2C_Read_Data(HTS_ADDR, HTS_HUM_OUT_L);
	buffer[1] = I2C_Read_Data(HTS_ADDR, HTS_HUM_OUT_H);

	H_T_out = (((uint16_t) buffer [1]) << 8) | (uint16_t) buffer [0];

	tmp_f = (float) (H_T_out - H0_T0_out) * (float) (H1_rh - H0_rh) / (float) (H1_T0_out - H0_T0_out) + H0_rh;

	result = (tmp_f> 100.0f)? 100.0f : (tmp_f <0.0f)? 0.0f : tmp_f;
	return result;
}


